# Atlassian Confluence Server with MySQL
This is a simple extension of the official Confluence Docker image with the MySQL Connector/J already embedded.
 
Find out more about Confluence at <https://www.atlassian.com/software/confluence>

You can find the repository for this Dockerfile at <https://bitbucket.org/insanitydesign/docker-confluence-server-mysql>
 
## Configuration
For all inherited configuration options check out the original Confluence Docker image at <https://hub.docker.com/r/atlassian/confluence-server/>

## Known Issues
Because of <https://jira.atlassian.com/browse/CONFSERVER-55399> the current MySQL Connector/J in this image is set to 5.x. As soon as the issue gets fixed, I will update to 8.x.
 
## Product Support
For Confluence product support go to [support.atlassian.com](http://support.atlassian.com).

## Disclaimer
I am not related to Atlassian or Confluence and do not explicitly endorse any relation. This source and the whole package comes without warranty. It may or may not harm your computer, server etc. Please use with care. Any damage cannot be related back to the author. The source has been tested on a virtual environment. Use at your own risk.

## Personal Note
I don't know if this is very useful for a lot of people but I required what is provided here. I hope this proves useful to you... with all its Bugs and Issues ;) If you like it you can give me a shout at [INsanityDesign](https://insanitydesign.com) or let me know via the repository.